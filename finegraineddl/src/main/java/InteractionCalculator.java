import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.lemurproject.galago.core.index.stats.FieldStatistics;
import org.lemurproject.galago.core.index.stats.IndexPartStatistics;
import org.lemurproject.galago.core.index.stats.NodeStatistics;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.NodeParameters;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;
import org.deeplearning4j.models.word2vec.Word2Vec;

import java.io.File;
import java.util.Hashtable;
import java.util.Map;


public class InteractionCalculator {

    double epsilon;
    long MAX_DOC_LEN = 1000;
    long docCount;
    Retrieval retrieval;
    Word2Vec vec;
    NodeStatistics dTerm1;
    NodeStatistics dBigram;
    NodeStatistics qdCooccerance;
    NodeStatistics qTerm;


    public static Map<String, NodeStatistics> bigCache;
    public Map<String, NodeStatistics> docCache;
    public static int freqOfIncachVals = 4;


    public InteractionCalculator(String pathIndexBase, String w2vecFile) throws Exception {
        retrieval = RetrievalFactory.instance( pathIndexBase );

        Node fieldNode = StructuredQuery.parse( "#lengths:part=lengths()" );
        FieldStatistics fieldStats = retrieval.getCollectionStatistics(fieldNode);
        IndexPartStatistics ps = retrieval.getIndexPartStatistics("postings");
        System.out.println("vocab size: " + ps.vocabCount);
        docCount = fieldStats.documentCount;


        File gModel = new File(w2vecFile);
//        vec = WordVectorSerializer.readWord2VecModel(gModel);

        epsilon = 0.0001 / this.docCount;
        bigCache = new Hashtable<>(20000000); // hashtable is thread safe
        docCache = new Hashtable<>(800000);
    }

    public void setTerms(String qTerm, String dTerm1, String dTerm2) throws Exception {
        NodeParameters np = new NodeParameters();
        qTerm = np.escapeAsNecessary(qTerm, true);
        dTerm1 = np.escapeAsNecessary(dTerm1, true);
        dTerm2 = np.escapeAsNecessary(dTerm2, true);

        this.dTerm1 = getNodeStats(dTerm1);
        this.qTerm = getNodeStats(qTerm);
        this.dBigram = getBigramNodeStats(dTerm1, dTerm2);
        this.qdCooccerance = getDocLevelNodeStats(qTerm, dTerm1);

//        System.out.println(qTerm + ": " + this.qTerm.nodeDocumentCount +
//                            dTerm1 + ": " + this.dTerm1.nodeDocumentCount +
//                            "bigram: " + dTerm1 + "-" + dTerm2 + this.dBigram.nodeDocumentCount +
//                            "cooccur: " + qTerm + "-" + dTerm1 + this.qdCooccerance.nodeDocumentCount);
    }

    private NodeStatistics getDocLevelNodeStats(String term1, String term2) throws Exception {
        String cacheKey = term1 + "." + term2;
        NodeStatistics res = docCache.getOrDefault(cacheKey, null);

        if (res == null) {
            String first = "#extents:" + term1 + ":part=postings()";
            String second = "#extents:" + term2 + ":part=postings()";

            Node termNode = StructuredQuery.parse("#unordered:" + MAX_DOC_LEN + "(" + first + second + ")");
            res = retrieval.getNodeStatistics(termNode);

            if (res.nodeDocumentCount > 1) // we will probably see all occurences
                docCache.put(cacheKey, res);
        }
        return res;
    }

    private NodeStatistics getBigramNodeStats(String term1, String term2) throws Exception {
        String cacheKey = term1 + "." + term2;
        NodeStatistics res  = bigCache.getOrDefault(cacheKey, null);

        if (res == null) {
            String first = "#extents:" + term1 + ":part=postings()";
            String second = "#extents:" + term2 + ":part=postings()";

            Node termNode = StructuredQuery.parse("#ordered:1(" + first + second + ")");
            res = retrieval.getNodeStatistics(termNode);

            if (res.nodeDocumentCount > freqOfIncachVals)
                bigCache.put(cacheKey, res);
        }

        return res;
    }

    private NodeStatistics getNodeStats(String term) throws Exception {
        Node termNode = StructuredQuery.parse( "#counts:" + term + ":part=postings()" );
        return retrieval.getNodeStatistics(termNode);
    }

    public Double computeDf(){
//        System.out.println("df is: " + this.dTerm1.nodeDocumentCount);
        return Math.log(this.dTerm1.nodeDocumentCount + 1);
    }

    public Double computeW2vecSimilarity(String term1, String term2) {
//        System.out.println(term1 + " " + term2 + " sim: " + vec.similarity(term1, term2));
        return vec.similarity(term1, term2);
    }

    public Double computeIdf() {
//        System.out.println("idf: " + Math.log((docCount + 1.0)/ (this.dTerm1.nodeDocumentCount + 1.0)));
        return Math.log((docCount  + 1.0)/ (this.dTerm1.nodeDocumentCount + 1.0));
    }

    public Double computeBigramDF() {
//        System.out.println(" bigdf: " + this.dBigram.nodeDocumentCount);
        return Math.log(this.dBigram.nodeDocumentCount + 1);
    }

    public Double computeFirstTermDF() {
//        System.out.println(" firstTermdf: " + this.dBigram.nodeDocumentCount);
        return Math.log(this.dBigram.nodeDocumentCount + 1);
    }

    public Double computeBigramIdf() {
//        System.out.println(" firstTermdf: " + this.dBigram.nodeDocumentCount);
        return Math.log( (docCount + 1) / this.dBigram.nodeDocumentCount + 1);
    }

    public Double computeMI(){
        Double res = 0.0;
        double both = this.qdCooccerance.nodeDocumentCount;
        double none = docCount - this.dTerm1.nodeDocumentCount - this.qTerm.nodeDocumentCount + both;
        double onlyQuery = this.qTerm.nodeDocumentCount - both;
        double onlyDoc = this.dTerm1.nodeDocumentCount - both;

        res += (both/this.docCount) * Math.log( (both * this.docCount + epsilon)
                /(this.qTerm.nodeDocumentCount * this.dTerm1.nodeDocumentCount + epsilon) );
        res += ( none/this.docCount) * Math.log( (none * this.docCount + epsilon)
                /((this.docCount - this.qTerm.nodeDocumentCount) * (this.docCount - this.dTerm1.nodeDocumentCount) + epsilon) );
        res += (onlyQuery/this.docCount) * Math.log( (onlyQuery * this.docCount + epsilon)
                /(this.qTerm.nodeDocumentCount * (this.docCount - this.dTerm1.nodeDocumentCount) + epsilon) );
        res += (onlyDoc/this.docCount) * Math.log( (onlyDoc * this.docCount + epsilon)
                /((this.docCount - this.qTerm.nodeDocumentCount) * this.dTerm1.nodeDocumentCount + epsilon) );

//        System.out.println("MI: " + res);
        return res;
    }

    public double getQueryTermidf(String term) throws Exception {
        NodeParameters np = new NodeParameters();
        term = np.escapeAsNecessary(term, true);
        NodeStatistics nodeStat = getNodeStats(term);
        return Math.log(docCount / (nodeStat.nodeDocumentCount + 0.5));
    }
}
