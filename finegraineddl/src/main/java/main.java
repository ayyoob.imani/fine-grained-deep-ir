import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class main {
    public static String dataDirectory = "./data/";
    public static String queryFile = "./data/topics.galago.json";
    public static String queryDocFile = "./data/qrels.adhoc.6y";
    public static String IndexBasePath = "";
    public static String w2vecPath = "";



    private static Map<Integer, List<String>> readQeuryDocPairs(String queryDocFile) throws IOException {
        Map<Integer, List<String>> res = new HashMap<>();

        String line;
        Integer qid;
        String doc;
        BufferedReader br = new BufferedReader(new FileReader(queryDocFile));

        while ((line = br.readLine()) != null) {
            String[] parts = line.split(" ");
            qid = Integer.parseInt(parts[0]);
            doc = parts[2];
            if (!res.containsKey(qid)){
                res.put(qid, new ArrayList<>());
            }
            res.get(qid).add(doc);
        }

        return res;
        }



    public static void  main(String[] args) throws Exception{
        setupArgs(args);
        Map<Integer, List<String>> docQueries =  readQeuryDocPairs(queryDocFile);
        QueryParser qParser = new QueryParser(queryFile, "galago");
        InteractionCalculator ic = new InteractionCalculator(IndexBasePath, w2vecPath);
        DocumentIterator documentIterator = new DocumentIterator(IndexBasePath, ic, dataDirectory);

        for (Integer qid : docQueries.keySet()){
            List<String> qTerms = qParser.getQueryTerms(qid);
            System.out.println("queyr: " + qid.toString());
            for (String docName : docQueries.get(qid)) {
                System.out.println("queyr: " + qid.toString() + " doc: " + docName);

                Map<String, Double[][]> featureMatrics = documentIterator.iterateDoc(qTerms, docName, qid);

                if (featureMatrics != null)
                    writeMatrices(qid, docName, featureMatrics);
            }

            ic.docCache.clear();
            clearBigramCache();

            BufferedWriter idfWriter = new BufferedWriter(new FileWriter(dataDirectory + "/" + qid.toString() + ".idf"));
            for (String qTerm : qTerms) {
                idfWriter.write(ic.getQueryTermidf(qTerm) + ",");
            }
            idfWriter.close();

        }

        documentIterator.finalize();
    }


//    public void computeAllSimilarity(String fileName) throws FileNotFoundException, Exception {
//        fileName += ".allsim-final";
//        File file = new File(fileName);
//        if (file.exists()) {
//            Scanner in = new Scanner(file);
//            while (in.hasNext()) {
//                String term = in.next();
//                double sim = in.nextDouble();
//                allSimilarity.put(term, sim);
//            }
//            in.close();
//        }
//        int threadNum = Runtime.getRuntime().availableProcessors() - 3;
//        System.out.println(threadNum + " threads are running for computing embedding similarities.");
//        ExecutorService exec = Executors.newFixedThreadPool(threadNum);
//        boolean flag = false;
//        try {
//            for (final String term : embeddingVectors.keySet()) {
//                if (allSimilarity.containsKey(term))
//                    continue;
//                flag = true;
//                exec.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            double sum = 0;
//                            for (String str : embeddingVectors.keySet()) {
//                                if (term.equals(str))
//                                    continue;
//                                sum += getSimilarity(term, str);
//                            }
//                            if (sum == 0) {
//                                throw new Exception("Something wrong happened!");
//                            }
//                            allSimilarity.put(term, sum);
//                            if (allSimilarity.size() % 100 == 0)
//                                logger.info("Similarity between " + allSimilarity.size() + " out of " + embeddingVectors.size() + " embedding vectors and all embedding other vectors have been so far computed!");
//                        } catch (Exception ex) {
//                            ex.printStackTrace();
//                        }
//                    }
//                });
//            }
//        } finally {
//            exec.shutdown();
//        }
//        int hour = 20;
//        logger.info("waiting for at most " + hour + " hours to compute these similarities.");
//        boolean finished = exec.awaitTermination(hour, TimeUnit.HOURS);
//        logger.info("All threads are terminated with this status: " + finished);
//
//        if (flag) {
//            PrintWriter pw = new PrintWriter(new File(fileName));
//            Iterator<Map.Entry<String, Double>> it = allSimilarity.entrySet().iterator();
//            while (it.hasNext()) {
//                Entry entry = it.next();
//                pw.println(entry.getKey() + " " + entry.getValue());
//            }
//            pw.close();
//        }
//        logger.info("Similarity between all embedding vectors has been computed!");
//    }


    synchronized private static void  clearBigramCache()  {
        if(InteractionCalculator.bigCache.size() > 20000000){
            InteractionCalculator.bigCache.clear();
            InteractionCalculator.freqOfIncachVals += 2;
        }
    }

    private static void writeMatrices(Integer qid, String docName, Map<String, Double[][]> featureMatrics) throws IOException {
        for (String fname : featureMatrics.keySet()) {
            createDirectory(dataDirectory + "/" + fname + "/" + qid.toString());
            BufferedWriter idfWriter = new BufferedWriter(new FileWriter(dataDirectory + "/" + fname + "/" + qid.toString() + "/" + docName));
            Double[][] vals = featureMatrics.get(fname);


            for (int i = 0; i < vals.length; i++) {
                for (int j = 0; j < vals[i].length; j++) {
                    idfWriter.write(vals[i][j].toString() + ",");
                }
                idfWriter.newLine();
            }
            idfWriter.close();
        }
    }

    private static void createDirectory(String directoryName) {
        File directory = new File(directoryName);
        if (! directory.exists()) {
            directory.mkdirs();
        }
    }

    public static void setupArgs(String[] args){
        System.out.println("run like: java -jar fine-grained-deep-ir.jar \"<path to index>\"");
        IndexBasePath = args[0];
        w2vecPath = args[1];
    }
}
