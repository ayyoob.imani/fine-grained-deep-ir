import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Created by vandermonde on 1/4/19.
 */
public class DocumentIterator {

    public static class Features{
        public static String DFS = "dfs";
        public static String IDFS = "idfs";
        public static String BIGDFS = "bigdfs";
        public static String BIGIDF = "bigidfs";
        public static String MIS = "mis";
        public static String SIMS = "sims";
    }


    public Retrieval index;
    private InteractionCalculator iCalulator;
    private static BufferedWriter NaNWriter;

    public DocumentIterator(String baseIndexPath, InteractionCalculator iCalculator, String dataDirectory) throws Exception {
        index = RetrievalFactory.instance(baseIndexPath);
        this.iCalulator = iCalculator;
        NaNWriter = new BufferedWriter(new FileWriter(dataDirectory + "/NANSIMS.txt" ));
    }


    public Map<String, Double[][]> iterateDoc(List<String> qTerms, String documentName, Integer qid) throws Exception {
        Document.DocumentComponents dc = new Document.DocumentComponents( false, false, true );
        Document doc = index.getDocument( documentName, dc );

        if ( doc == null) {
            System.err.println("warning: couldn't find document: " + documentName);
            return null;
        }

        Map<String, Double[][]> res = new HashMap<>();
        setupMatrices(qTerms, doc, res);

        ListIterator<String> it = qTerms.listIterator();
        while (it.hasNext()){
            int qtermPos = it.nextIndex();
            String qTerm = it.next();

            for (int position = 0; position < doc.terms.size()-1; position++) {
                String dTerm1 = doc.terms.get(position);
                String dTerm2 = doc.terms.get(position + 1);
                calculateStats(res, qTerm, dTerm1, dTerm2, qtermPos, position, false, documentName, qid);
            }
            String dTerm1 = doc.terms.get(doc.terms.size()-1);
            calculateStats(res, qTerm, dTerm1, "temp", qtermPos,doc.terms.size()-1, true, documentName, qid);
        }


        System.out.println("big cache size: " + iCalulator.bigCache.size());
        System.out.println("doc cache size: " + iCalulator.docCache.size());

        return res;
    }

    private void setupMatrices(List<String> qTerms, Document doc, Map<String, Double[][]> res) {
        Double[][] dfs = new Double[qTerms.size()][doc.terms.size()];
        res.put(Features.DFS, dfs);

        Double[][] idfs = new Double[qTerms.size()][doc.terms.size()];
        res.put(Features.IDFS, idfs);

        Double[][] MIs = new Double[qTerms.size()][doc.terms.size()];
        res.put(Features.MIS, MIs);

        Double[][] BigDfs = new Double[qTerms.size()][doc.terms.size()];
        res.put(Features.BIGDFS, BigDfs);

        Double[][] sims = new Double[qTerms.size()][doc.terms.size()];
        res.put(Features.SIMS, sims);

        Double[][] bigidfs = new Double[qTerms.size()][doc.terms.size()];
        res.put(Features.BIGIDF, bigidfs);
    }

    public void calculateStats(Map<String, Double[][]> res, String qterm, String dterm1, String dterm2, int qPos, int dpos, boolean isend, String docName, Integer qid) throws Exception {
        iCalulator.setTerms(qterm, dterm1, dterm2);

        res.get(Features.DFS)[qPos][dpos] = iCalulator.computeDf();
        res.get(Features.IDFS)[qPos][dpos] = iCalulator.computeIdf();
        res.get(Features.SIMS)[qPos][dpos] = iCalulator.computeW2vecSimilarity(qterm, dterm1);
        res.get(Features.MIS)[qPos][dpos] = iCalulator.computeMI();

        if(dpos == 0) {
            res.get(Features.BIGDFS)[qPos][0] = iCalulator.computeFirstTermDF();
            res.get(Features.BIGIDF)[qPos][0] = iCalulator.computeBigramIdf();
        }
        if (!isend) {
            res.get(Features.BIGDFS)[qPos][dpos + 1] = iCalulator.computeBigramDF();
            res.get(Features.BIGIDF)[qPos][dpos + 1] = iCalulator.computeBigramIdf();
        }

        if (res.get(Features.SIMS)[qPos][dpos].isNaN())
            NaNWriter.write( qid.toString() + "," + docName + "," + qPos + "," + dpos + "," + qterm + "," + dterm1 + "\n");
    }

    public void finalize() throws IOException {
        NaNWriter.close();
    }
}
