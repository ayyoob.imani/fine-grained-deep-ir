import org.lemurproject.galago.core.parse.Document;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.parse.Tag;

import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Created by vandermonde on 1/4/19.
 */
public class DocumentIterator {
    public Retrieval index;
    private InteractionCalculator iCalulator;

    public DocumentIterator(String baseIndexPath, InteractionCalculator iCalculator) throws Exception {
        index = RetrievalFactory.instance(baseIndexPath);
        this.iCalulator = iCalculator;
    }

    public Map<String, Double[][]> iterateDoc(List<String> qTerms, String documentName) throws Exception {
        Document.DocumentComponents dc = new Document.DocumentComponents( false, false, true );
        Document doc = index.getDocument( documentName, dc );

        Map<String, Double[][]> res = new HashMap<>();
        Double[][] dfs = null;

        for ( Tag tag : doc.tags ) {
            if ( tag.name.equals( "text" ) ) {
                dfs = new Double[qTerms.size()][tag.end - tag.begin];
                ListIterator<String> it = qTerms.listIterator();
                while (it.hasNext()){
                    int qtPos = it.nextIndex();
                    String qTerm = it.next();
                    for (int position = tag.begin; position < tag.end; position++) {
                        String dTerm = doc.terms.get(position);
                        dfs[qtPos][position - tag.begin] = iCalulator.computeDf(qTerm, dTerm);
                    }
                }
            }
        }

        res.put("dfs", dfs);
        return res;
    }
}
