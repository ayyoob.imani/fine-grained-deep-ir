import org.lemurproject.galago.core.index.stats.FieldStatistics;
import org.lemurproject.galago.core.index.stats.NodeStatistics;
import org.lemurproject.galago.core.retrieval.Retrieval;
import org.lemurproject.galago.core.retrieval.RetrievalFactory;
import org.lemurproject.galago.core.retrieval.query.Node;
import org.lemurproject.galago.core.retrieval.query.StructuredQuery;

public class InteractionCalculator {

    long docCount;
//    String pathIndexBase = "/home/jiepu/Downloads/example_index_galago/"; //TODO fix me
    Retrieval retrieval;


    public InteractionCalculator(String pathIndexBase) throws Exception {
        retrieval = RetrievalFactory.instance( pathIndexBase );
        Node fieldNode = StructuredQuery.parse( "#lengths:text:part=lengths()" );
        FieldStatistics fieldStats = retrieval.getCollectionStatistics(fieldNode);
        docCount = fieldStats.documentCount;
    }

    public Double computeDf(String term1, String term2) throws Exception {
        Double res = 0D;
        NodeStatistics termStats = retrieval.getNodeStatistics(term1);
        res = Math.log((termStats.nodeDocumentCount + 1.0)/ (docCount + 1.0));
        return res;
    }


}
