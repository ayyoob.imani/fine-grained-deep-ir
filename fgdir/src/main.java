import org.lemurproject.galago.core.index.disk.DiskIndex;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class main {
    public static String dataDirectory = "./data/";
    public static String queryFile= "./qrels.adhoc.6y";
    public static String IndexBasePath = "";

    private static Map<Integer, List<String>> readQeuryDocPairs(String queryDocFile) throws IOException {
        Map<Integer, List<String>> res = new HashMap<>();

        String line;
        Integer qid;
        String doc;
        BufferedReader br = new BufferedReader(new FileReader(queryDocFile));

        while ((line = br.readLine()) != null) {
            String[] parts = line.split(" ");
            qid = Integer.parseInt(parts[0]);
            doc = parts[2];
            if (!res.containsKey(qid)){
                res.put(qid, new ArrayList<>());
            }
            res.get(qid).add(doc);
        }

        return res;
        }



    public static String queryDocFile = "./data/";

    public static void  main(String[] args) throws Exception{
        Map<Integer, List<String>> docQueries =  readQeuryDocPairs(queryDocFile);
        QueryParser qParser = new QueryParser(queryFile);
        InteractionCalculator ic = new InteractionCalculator(IndexBasePath);
        DocumentIterator documentIterator = new DocumentIterator(IndexBasePath, ic);

        for (Integer qid : docQueries.keySet()){

            List<String> qTerms = qParser.getQueryTerms(qid);
            createDirectory(dataDirectory + "/idf/" + qid.toString());

            for (String docName : docQueries.get(qid)) {
                Map<String, Double[][]> featureMatrics = documentIterator.iterateDoc(qTerms, docName);
                writeMatrices(qid, docName, featureMatrics);
            }
        }
    }

    private static void writeMatrices(Integer qid, String docName, Map<String, Double[][]> featureMatrics) throws IOException {
        BufferedWriter idfWriter = new BufferedWriter(new FileWriter(dataDirectory + "/idf/" + qid.toString() + "/" + docName));
        Double[][] idfs = featureMatrics.get("idfs");

        for (int i = 0; i < idfs.length; i++) {
            for (int j = 0; i < idfs[i].length; j++) {
                idfWriter.write(idfs[i][j].toString() + ",");
            }
            idfWriter.newLine();
        }
        idfWriter.close();
    }

    private static void createDirectory(String directoryName) {
        File directory = new File(directoryName);
        if (! directory.exists()) {
            directory.mkdirs();
        }
    }

}
