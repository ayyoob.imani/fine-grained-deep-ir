import com.sun.org.apache.xml.internal.utils.WrongParserException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javafx.util.Pair;
import org.lemurproject.galago.core.retrieval.query.StructuredLexer;
import org.w3c.dom.Document;
import java.io.StringReader;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.lemurproject.galago.utility.Parameters;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class QueryParser {
    private Map<Integer, List<String>> queries;

    public QueryParser(String queryFile) throws IOException, ParserConfigurationException, SAXException {
        queries = setupQueries(queryFile);
    }

    public Map <Integer, List<String>> setupQueries (String fileName) throws IOException, FileNotFoundException, ParserConfigurationException, SAXException {
        List<Pair<String, String>> queries = readTrecQueries(fileName);

        Map<Integer, List<String>> res = new HashMap<>();
        for (Pair<String, String> query: queries){
            ArrayList<StructuredLexer.Token> tokens = StructuredLexer.tokens(query.getValue());
            ArrayList<String> terms = new ArrayList<>();
            for(StructuredLexer.Token token: tokens){
                terms.add(token.text);
            }
            res.put(Integer.parseInt(query.getKey()), terms);
        }

        return res;
    }


    public List <Pair<String, String>> readTrecQueries (String fileName) throws FileNotFoundException, ParserConfigurationException, SAXException, IOException{
        List <Pair <String, String>> queries = new ArrayList <> ();
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        String xmlRecords = new Scanner(new File(fileName)).useDelimiter("\\Z").next();
        xmlRecords = "<data>" + xmlRecords + "</data>";
        xmlRecords = xmlRecords.replaceAll("&", "&amp;");
        is.setCharacterStream(new StringReader(xmlRecords));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("DOC");

        // iterate the employees
        for (int i = 0; i < nodes.getLength(); i++) {
            Element element = (Element) nodes.item(i);

            NodeList qNum = element.getElementsByTagName("DOCNO");
            Element line = (Element) qNum.item(0);
            String queryNumber = getCharacterDataFromElement(line).trim();

            NodeList title = element.getElementsByTagName("TEXT");

            line = (Element) title.item(0);
            String queryText = getCharacterDataFromElement(line).trim();
            while (queryText.endsWith(".")){ // because of Galago bug in StructuredQuery parsing
                queryText = queryText.substring(0, queryText.length()-1);
            }
            //System.err.println("~~~~ " + queryNumber + ": " + queryText);
            queries.add(new Pair(queryNumber, queryText));
        }
        System.out.println(queries.size() + " queries have been read from file.");
        return queries;
    }

    public static List <Parameters> getGalagoFormatQueries (List <Pair <String, String>> inputQueries) throws IOException{
        List <Parameters> queries = new ArrayList <> ();
        for (Pair q : inputQueries){
            queries.add(Parameters.parseString(String.format("{\"number\":\"%s\", \"text\":\"%s\"}", q.getKey(), q.getValue())));
        }
        return queries;
    }

    public static String getCharacterDataFromElement(Element e) throws WrongParserException {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        throw new WrongParserException("WRONG TREC FORMAT QUERY!");
    }

    public List<String> getQueryTerms(Integer qid){
        return queries.get(qid);
    }
}